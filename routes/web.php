<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/Product-list', 'WelcomeController@product');

Route::get('/Brand-list', 'WelcomeController@brand');

Route::get('/Contact', 'WelcomeController@contact');

Route::resource('brands', 'BrandController' );

//Route::get('/singleproduct/{id}', function () {
//    return view('welcome');
//});

