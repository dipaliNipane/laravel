<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index() {
       
       $title = "Welcome to Estore!";
       return view('welcome.index')->with('title', $title); 
    }
    
    public function product() {
       
       $title = "Product List";
       return view('welcome.product')->with('title', $title); 
    }
    
    public function brand() {
       
       $title = "Brands we sell";
       return view('welcome.brand')->with('title', $title); 
    }
    
    public function contact() {
       
       $title = "Contact us!";
       return view('welcome.contact')->with('title', $title); 
    }
}

























