<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/">Estore</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="/">Home</a></li>
      <li><a href="/Product-list">Products</a></li>
      <li><a href="/Brand-list">Brands</a></li>
      <li><a href="/Contact">Contact</a></li>
    </ul>
  </div>
</nav>