@extends('layout.app')
@section('content')
<div class="jumbotron text-center">
    <h1>{{$title}}</h1>
    <p>Combining 100% pure fabric with unprecedented design asthetic to create designer wear at an affordable price.</p>
    <div class="btn-group">
        <button type="button" class="btn btn-primary">Go to products</button>
        <button type="button" class="btn btn-primary">Contact Us</button>
        
      </div>
</div>
    
@endsection